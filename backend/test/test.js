process.env.NODE_ENV = 'test'
process.env.PORT = 3050

const chai = require('chai');
const chaiHttp = require('chai-http')
const assert = require('assert')
const sinon = require('sinon')
const nodemailer = require('nodemailer')

let server = require('../app')
let should = chai.should()
const mock = require('mock-require')
const sendMail = sinon.spy()


chai.use(chaiHttp);

describe('Event', function() {
  let mocks;
  before( () => {
    mocks = sinon.stub(nodemailer, 'createTransport').returns(
      {
        sendMail
      }
    )
  })
  after( () => {
    nodemailer.createTransport.restore()
  })
  describe('When I try add event ', function() {
    it('should return 201', function(done) {
      chai.request(server)
        .post('/add')
        .send({ name: 'test', address: 'Test Street', phone: '555 - 55555', time: '12:30', date: '2019-12-12'})
        .end((err, res) => {
          res.should.have.status(201);
          done();
        });
    });
    it('should return 400', function(done) {
      chai.request(server)
        .post('/add')
        .send({ address: 'Test Street', phone: '555 - 55555', time: '12:30', date: '2019-12-12'})
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
});
