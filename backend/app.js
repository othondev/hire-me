const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const router = require('./routes')
const { NODE_ENV } = process.env
const { logger } = require('./services/logger')

const app = express();

const noCors = (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
}

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

logger.info(`run in ${NODE_ENV} mode`)
if(NODE_ENV !== 'production'){
  app.use(noCors)
  require('dotenv').config()
  app.get('/', (req, res) => res.redirect('http://localhost:8080'))
} else {
  app.use(express.static(__dirname + '/dist'));
}

app.use('/', router);

module.exports = app;
