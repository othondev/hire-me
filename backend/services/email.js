const nodemailer = require("nodemailer");

const _createTransporter = () =>{
const { EMAIL_PASSWORD, EMAIL_USER } = process.env
  return nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: EMAIL_USER,
      pass: EMAIL_PASSWORD
    }
  });
}

const _getTemplate = ({ date, time, address, name, phone }) =>{
  return `
    Day: ${date} at ${time}\n
    Location: ${address}\n

    Phone: ${phone} - (${name})
  `
}

module.exports.sendEmail = async ( { event } ) => {
  const mailOptions = {
    from: 'othon.au@gmail.com',
    to: 'othon.au@gmail.com',
    subject: 'URGENT: Opportunity Job',
    html: _getTemplate(event)
  };
  
  return _createTransporter().sendMail(mailOptions)
}
