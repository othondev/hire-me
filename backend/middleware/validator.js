module.exports.eventValidator = (req, res, next) => {
  const { name, address, phone, time, date } = req.body
  if( !!name && !!address && !!phone && !!time && !!date ) {
    next()
  }else{
    res.status(400).json({ message: 'Invalid parameters' 
    })
  }
}
