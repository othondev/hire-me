const express = require('express');
const router = express.Router();
const winston = require('winston')
const { eventValidator } = require('../middleware/validator')
const { sendEmail } = require('../services/email')
const { version } = require('../package.json')
const { logger } = require('../services/logger')

router.get('/heathcheck', (req, res) => {
  res.json({ version })
});

router.post('/add', eventValidator, (req, res) => {
  sendEmail({ event: req.body })
    .then( result => {
      logger.info('Message sent')
      res.status(201).json({ message: 'Event added' })
    })
    .catch( err => {
      logger.error(`Error: ${err} `)
      res.status(500).json({ err })
    })
});

module.exports = router;
